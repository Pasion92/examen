package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.examen.ViewModels.Registrar;
import com.example.examen.ViewModels.Servicio;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PantallaRegistrar extends AppCompatActivity {

    public Button registrar;
    public EditText user, pass1, pass2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_registrar);
        user = (EditText) findViewById(R.id.user);
        pass1 = (EditText) findViewById(R.id.pass1);
        pass2 = (EditText) findViewById(R.id.pass2);
        registrar = (Button) findViewById(R.id.registrar);
        String hola ="";
        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!user.getText().toString().isEmpty() && !pass1.getText().toString().isEmpty() && !pass2.getText().toString().isEmpty()){
                    if (pass1.getText().toString().equals(pass2.getText().toString())){
                        Api api = new Api();
                        Servicio service = api.getApi(PantallaRegistrar.this).create(Servicio.class);
                        Call<Registrar> registrarCall =  service.registrarUsuario(user.getText().toString(),pass2.getText().toString());
                        registrarCall.enqueue(new Callback<Registrar>() {
                            @Override
                            public void onResponse(Call<Registrar> call, Response<Registrar> response) {
                                Registrar peticion = response.body();
                                if(response.body() == null){
                                    Toast.makeText(PantallaRegistrar.this, "Ocurrio un Error, intentalo más tarde", Toast.LENGTH_LONG).show();
                                    return;
                                }
                                if(peticion.estado == "true"){
                                    startActivity(new Intent(PantallaRegistrar.this,MainActivity.class));
                                    Toast.makeText(PantallaRegistrar.this, "Datos Registrador", Toast.LENGTH_LONG).show();
                                }else{
                                    Toast.makeText(PantallaRegistrar.this, peticion.detalle, Toast.LENGTH_LONG).show();
                                }
                            }
                            @Override
                            public void onFailure(Call<Registrar> call, Throwable t) {
                                Toast.makeText(PantallaRegistrar.this, "Erro :(", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        Toast.makeText(PantallaRegistrar.this, "Contraseñas diferentes", Toast.LENGTH_SHORT).show();
                    }
                } else{
                    Toast.makeText(PantallaRegistrar.this, "Campos vacíos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
