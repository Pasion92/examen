package com.example.examen.ViewModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Disco {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("nombre")
    @Expose
    private String nombre;
    @SerializedName("album")
    @Expose
    private String album;
    @SerializedName("anio")
    @Expose
    private String anio;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Disco withId(Integer id) {
        this.id = id;
        return this;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Disco withNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public Disco withAlbum(String album) {
        this.album = album;
        return this;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public Disco withAnio(String anio) {
        this.anio = anio;
        return this;
    }

}
