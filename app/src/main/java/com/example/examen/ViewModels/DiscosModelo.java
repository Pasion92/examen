package com.example.examen.ViewModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DiscosModelo {

    @SerializedName("estado")
    @Expose
    private Boolean estado;
    @SerializedName("discos")
    @Expose
    private List<Disco> discos = null;

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public DiscosModelo withEstado(Boolean estado) {
        this.estado = estado;
        return this;
    }

    public List<Disco> getDiscos() {
        return discos;
    }

    public void setDiscos(List<Disco> discos) {
        this.discos = discos;
    }

    public DiscosModelo withDiscos(List<Disco> discos) {
        this.discos = discos;
        return this;
    }

}
