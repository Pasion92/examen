package com.example.examen.ViewModels;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Servicio {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registrar> registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/loginSocial")
    Call<Loguear> getLoginPractica(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/detalleDisco")
    Call<DiscosModelo> getDetalles(@Field("discoId") int id);

    @POST("api/todosDiscos")
    Call<DiscosModelo> getDiscos();
}
