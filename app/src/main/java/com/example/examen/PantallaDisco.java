package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.examen.ViewModels.Disco;
import com.example.examen.ViewModels.DiscosModelo;
import com.example.examen.ViewModels.Servicio;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PantallaDisco extends AppCompatActivity {

    List<Disco> discos;
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_disco);

        discos = new ArrayList<>();
        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerAdapter = new RecyclerAdapter(getApplicationContext(), discos);
        recyclerView.setAdapter(recyclerAdapter);

        Servicio service = Api.getApi(PantallaDisco.this).create(Servicio.class);
        Call<DiscosModelo> noticias =  service.getDiscos();
        noticias.enqueue(new Callback<DiscosModelo>() {
            @Override
            public void onResponse(Call<DiscosModelo> call, Response<DiscosModelo> response) {

                DiscosModelo user= response.body();
                if (!PantallaDisco.this.isFinishing()){
                    if (user.getEstado()){
                        recyclerAdapter.SetUsuarios(user.getDiscos());
                    } else
                        Toast.makeText(PantallaDisco.this, "Petición no procesada", Toast.LENGTH_LONG).show();;
                }

            }

            @Override
            public void onFailure(Call<DiscosModelo> call, Throwable t) {
                Log.d("TAG","Response = "+t.toString());
                Toast.makeText(PantallaDisco.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                t.printStackTrace();
            }
        });

    }
}
