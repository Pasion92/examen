package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.examen.ViewModels.Disco;
import com.example.examen.ViewModels.DiscosModelo;
import com.example.examen.ViewModels.Servicio;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PantallaDetalle extends AppCompatActivity {

    TextView id, pass;
    String parametro, cod;
    List<Disco> losdiscos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_detalle);
        id = (TextView) findViewById(R.id.id);
        pass = (TextView) findViewById(R.id.pass);
        parametro = getIntent().getExtras().getString("parametro");
        cod = getIntent().getExtras().getString("cod");
        losdiscos = new ArrayList<>();


        Servicio service = Api.getApi(PantallaDetalle.this).create(Servicio.class);
        Call<DiscosModelo> loginCall = service.getDetalles(Integer.parseInt(cod));
        loginCall.enqueue(new Callback<DiscosModelo>() {
            @Override
            public void onResponse(Call<DiscosModelo> call, Response<DiscosModelo> response) {
                DiscosModelo peticion = response.body();
                if (peticion.getEstado()) {
                    id.setText(parametro+ "  id:"+cod);
                    //pass.setText(peticion.getDiscos().get(0).getNombre().toString());
                } else {
                    Toast.makeText(PantallaDetalle.this, "Datos Incorrectos", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<DiscosModelo> call, Throwable t) {
                Toast.makeText(PantallaDetalle.this, "Error :(", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
